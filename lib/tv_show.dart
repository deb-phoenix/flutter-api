
class TvShow {
  int id;
  String name;
  String imageThumbnailPath;

  TvShow({
    required this.id,
    required this.name,
    required this.imageThumbnailPath,
  });

  factory TvShow.fromJson(Map<String, dynamic> json) {
    return TvShow(
      id: json['id'],
      name: json['name'],
      imageThumbnailPath: json['image_thumbnail_path'],
    );
  }
}